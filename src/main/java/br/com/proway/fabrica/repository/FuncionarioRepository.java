package br.com.proway.fabrica.repository;

import br.com.proway.fabrica.model.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
public interface FuncionarioRepository extends JpaRepository <Funcionario, Long>{

    ArrayList<Funcionario> findAll();
    Funcionario findById(long id);
    Funcionario save(Funcionario funcionario);
    void deleteById(long id);


   @Query("SELECT f FROM Funcionario f Where f.id => :id ")
   ArrayList<Funcionario> findMaioresIds(@Param("id") long id);

}
