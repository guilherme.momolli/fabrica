package br.com.proway.fabrica.repository;

import br.com.proway.fabrica.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
public interface ClienteRepository extends JpaRepository <Cliente, Long> {

    ArrayList<Cliente> findAll();
    Cliente findById(long id);
    Cliente save(Cliente cliente);
    void deleteById(long id);


    @Query("SELECT c FROM Cliente c WHERE c.nome LIKE %:letra%")
    ArrayList<Cliente> findContemLetra(@Param("letra") String letra);


}
