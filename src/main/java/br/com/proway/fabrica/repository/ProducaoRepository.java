package br.com.proway.fabrica.repository;


import br.com.proway.fabrica.model.Funcionario;
import br.com.proway.fabrica.model.Producao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
public interface ProducaoRepository extends JpaRepository <Producao, Long> {

    ArrayList<Producao> findAll();
    Producao findById(long id);
    Producao save(Producao producao);
    void deleteById(long id);

   // @Query("SELECT p FROM Producao p Where p.preco => :demanda ")
  //  ArrayList<Producao> findMaiorDemanda(@Param("demanda")long demanda);

}
