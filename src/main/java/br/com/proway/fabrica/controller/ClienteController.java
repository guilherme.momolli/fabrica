package br.com.proway.fabrica.controller;


import br.com.proway.fabrica.model.Cliente;
import br.com.proway.fabrica.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class ClienteController{

    @Autowired
    private ClienteRepository clienteRepository;

    @GetMapping("/clientes")
    public ArrayList<Cliente> getAllClientes(){return clienteRepository.findAll();}

    @GetMapping("/cliente/{id}")
    public Cliente getCliente(@PathVariable("id")long id){return clienteRepository.findById(id);}

    @PostMapping("/cliente/add")
    public ResponseEntity<Cliente> addCliente(@RequestBody Cliente cliente){
        Cliente c;
        try{
            c = clienteRepository.save(cliente);
            return new ResponseEntity<>(c, HttpStatus.CREATED);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/clientes/{id}")
    public void deleteCliente(@PathVariable("id") long id){ clienteRepository.deleteById(id);}

    @PutMapping("/clientes/{id}")
    public Cliente atualizarCliente(@PathVariable("id") long id, @RequestBody Cliente cliente){
        cliente.setId(id);
        return clienteRepository.save(cliente);
    }

    @GetMapping("/clientescontemletra/{letra}")
    public ArrayList<Cliente> getClientesContemLetra(@PathVariable("letra") String letra){
        return clienteRepository.findContemLetra(letra);
    }

}
