package br.com.proway.fabrica.controller;

import br.com.proway.fabrica.model.Funcionario;
import br.com.proway.fabrica.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class FuncionarioController{

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @GetMapping("/funcionarios")
    public ArrayList<Funcionario> getAllFuncionarios(){return funcionarioRepository.findAll();}

    @GetMapping("/funcionario/{id}")
    public Funcionario getFuncionario(@PathVariable("id")long id){return funcionarioRepository.findById(id);}

    @PostMapping("/funcionario/add")
    public ResponseEntity<Funcionario> addFuncionario(@RequestBody Funcionario funcionario){
        Funcionario f;
        try{
            f = funcionarioRepository.save(funcionario);
            return new ResponseEntity<>(f, HttpStatus.CREATED);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/funcionarios/{id}")
    public void deleteFuncionarios(@PathVariable("id") long id){ funcionarioRepository.deleteById(id);}

    @PutMapping("/funcionarios/{id}")
    public Funcionario atualizarFuncionarios(@PathVariable("id") long id, @RequestBody Funcionario funcionario){
        funcionario.setId(id);
        return funcionarioRepository.save(funcionario);
    }

    @GetMapping("/funcionariomaiorid/{id}")
    public ArrayList<Funcionario> procudaraMaiorId(@PathVariable("id") @RequestBody long id){
       return funcionarioRepository.findMaioresIds(id);
    }

}
