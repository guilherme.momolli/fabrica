package br.com.proway.fabrica.controller;

import br.com.proway.fabrica.model.Funcionario;
import br.com.proway.fabrica.model.Producao;
import br.com.proway.fabrica.repository.ProducaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class ProducaoController{

    @Autowired
    private ProducaoRepository producaoRepository;

    @GetMapping("/producoes")
    public ArrayList<Producao> getAllProducoes(){return producaoRepository.findAll();}

    @GetMapping("/producao/{id}")
    public Producao getProducao(@PathVariable("id")long id){return producaoRepository.findById(id);}

    @PostMapping("/producao/add")
    public ResponseEntity<Producao> addProducao(@RequestBody Producao producao){
        Producao p;
        try{
            p = producaoRepository.save(producao);
            return new ResponseEntity<>(p, HttpStatus.CREATED);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/producao/{id}")
    public void deleteProducao(@PathVariable("id") long id){ producaoRepository.deleteById(id);}

    @PutMapping("/producao/{id}")
    public Producao atualizarProducao(@PathVariable("id") long id, @RequestBody Producao producao){
        producao.setId(id);
        return producaoRepository.save(producao);
    }

   // @PutMapping("/producaomaiordemanda/{demanda}")
 //   public ArrayList<Producao> procudaraMaiorId(@PathVariable("demanda") @RequestBody long demanda){
  //      return producaoRepository.findMaiorDemanda(demanda);
  //  }

}

