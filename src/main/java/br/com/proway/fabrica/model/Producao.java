package br.com.proway.fabrica.model;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "producao")
@EntityListeners(AuditingEntityListener.class)
public class Producao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "produto")
    private String produto;
    @Column(name = "demanda")
    private long demanda;
    @Column(name = "preco")
    private double preco;
}
